import { TestBed, inject } from '@angular/core/testing';

import { IncidentnavigationService } from './incidentnavigation.service';

describe('IncidentnavigationService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [IncidentnavigationService]
    });
  });

  it('should be created', inject([IncidentnavigationService], (service: IncidentnavigationService) => {
    expect(service).toBeTruthy();
  }));
});
