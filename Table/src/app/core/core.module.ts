import {NgModule, Optional, SkipSelf} from '@angular/core';
import { CommonModule } from '@angular/common';
import {SettingsService} from './settings/settings.service';
import { throwIfAlreadyLoaded } from './module-import-guard';
import {IncidentnavigationService} from './incidentnavigation/incidentnavigation.service';

@NgModule({
  imports: [
    CommonModule
  ],
  providers: [
    SettingsService,
    IncidentnavigationService
  ],
  declarations: []
})
export class CoreModule {
  constructor( @Optional() @SkipSelf() parentModule: CoreModule) {
    throwIfAlreadyLoaded(parentModule, 'CoreModule');
  }
}
