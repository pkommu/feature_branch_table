import { Component, OnInit, Input } from '@angular/core';
import {ActivatedRoute} from '@angular/router';

@Component({
  selector: 'app-common',
  templateUrl: './common.component.html',
  styleUrls: ['./common.component.scss']
})
export class CommonComponent implements OnInit {

  error: String;

  constructor(private route:ActivatedRoute) { 
    this.error = this.route.snapshot.params['error'];
  }

  ngOnInit() {

  }

}
