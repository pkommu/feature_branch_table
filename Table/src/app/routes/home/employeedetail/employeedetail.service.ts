import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class EmployeedetailService {

  constructor(private http: HttpClient) { }
  getHeaders() {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'transactionId': 'sample',
        'reportType': 'All'
      }),
      withCredentials: true
    };
    return httpOptions;
  }

  public getAll(payload) {
    return this.http.post("http://localhost:4200" + '/getAll',payload, this.getHeaders()).pipe(map((res: any) => res));
  }
}
