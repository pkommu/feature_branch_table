import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { EmployeedetailComponent } from './home/employeedetail/employeedetail.component';
import { route } from './route';
import {DataTableModule} from "angular-6-datatable";
import { HomeComponent } from './home/home.component';
import { NgScrollbarModule } from 'ngx-scrollbar';



@NgModule({
  imports: [
    NgScrollbarModule,
    CommonModule,
    RouterModule.forRoot(route),
    DataTableModule
     ],
  declarations: [
    HomeComponent,
    EmployeedetailComponent],
  exports: [],
  providers: [ ]})
export class RoutesModule { }
